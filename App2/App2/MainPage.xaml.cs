﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace App2
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void Btn1_Clicked(object sender, EventArgs e)
        {
            Hola();
        }

        int contador = 0;

        public void Hola()
        {
            string[] frases = new string[] { "frase 1", "frase 2", "frase 3", "frase 4" };

            //slider.Value = 0.5;

            Console.WriteLine("Mensaje en consola cuando presiona el botón");
            //label.Text = "CAMBIO!!!!";

            label.Text = frases[contador];

            contador = contador + 1;
            if (contador>=4)
            {
                contador = 0;
            }

        }

        private void Slider_ValueChanged(object sender, ValueChangedEventArgs e)
        {            
            label.FontSize = e.NewValue;
        }
    }

}
